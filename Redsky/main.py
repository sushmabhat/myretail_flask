#Author: Sushma Bhat
#This class contains the REST APIs to handle product details

import helper
from flask import Flask, request, Response
import json

app = Flask(__name__)


@app.route('/product/<id>/name')
def get_price(id):
    name = helper.get_name(id)
    print("Name:", name)
    # Return 404 if item not found
    if name is None:
        response = Response("{'error': 'Item Not Found - '}" + id, status=404, mimetype='application/json')
        return response

    # Return status
    res_data = {
        'name': name,
    }

    response = Response(json.dumps(res_data), status=200, mimetype='application/json')
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8002)

@app.route('/item/<name>/<price>/<currency>', methods=['POST'])
def add_item(name, price, currency):
    # Add item to the list
    res_data = helper.add_to_list(name, price, currency)

    # Return error if item not added
    if res_data is None:
        response = Response("{'error': 'Item not added - '}" + item, status=400, mimetype='application/json')
        return response

    # Return response
    response = Response(json.dumps(res_data), mimetype='application/json')

    return response



