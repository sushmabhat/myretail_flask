#Author: Sushma Bhat
#THis module makes two external calls to get the price and product information separatly, aggregates them and send the response

from flask import Flask, request, Response
import json
import requests

app = Flask(__name__)

@app.route('/products/<id>')
def get_prod_details(id):
    price_response = requests.get("http://127.0.0.1:8001/product/"+id+"/price")
    name_response = requests.get("http://127.0.0.1:8002/product/" + id + "/name")
    if price_response is None or name_response is None:
        response = Response("{'error': 'Item Not Found - '}" + id, status=404, mimetype='application/json')
        return response
    dict = {}
    name_json = json.loads(name_response.text)
    price_json = json.loads(price_response.text)
    response = {}
    response["id"] = id
    response["name"] = name_json["name"]
    response["current_price"] = price_json

    print("response:", response)
    res = Response(json.dumps(response), status=200, mimetype='application/json')
    return res


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8003)
